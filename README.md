## React Native Live Stream
Front-end of the Look Movie app with react native.

## Preview

<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=1mdV90_lxy5LFi97PMq6eajJ2IL3oWODS" />
<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=11x1k9ZRR-tvtFb-N49uB1g7wj29_n0es" />

## Run
```sh
git clone https://gitlab.com/superdev1008_xecure/lookmovie.git
cd lookmovie
npm install
react-native run-ios | react-native run-android
```